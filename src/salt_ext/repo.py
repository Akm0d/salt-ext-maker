async def __init__(hub):
    await hub.pop.sub.add(python_import="pygit2", sub=hub.lib)


async def clone(hub, salt_repo_url: str, salt_version: str, clone_dir: str, **kwargs):
    """
    Clone the salt repo with the given version or tag to the named directory
    """
    await hub.log.debug(f"Ensuring that clone directory exists: {clone_dir}")
    hub.lib.os.makedirs(clone_dir, exist_ok=True)

    try:
        repo = hub.lib.pygit2.Repository(clone_dir)

        # Check if salt_version is a branch or a tag
        if salt_version in repo.listall_branches():
            reference = f"refs/heads/{salt_version}"
        else:
            reference = f"refs/tags/{salt_version}"

        current_commit = repo.revparse_single("HEAD").hex
        target_commit = repo.revparse_single(reference).hex

        if current_commit == target_commit:
            await hub.log.debug(f"Repository already cloned and at desired version {salt_version}.")
            return repo
        else:
            await hub.log.debug(f"Repository exists but not at desired version. Checking out version {salt_version}.")
            repo.checkout(repo.lookup_reference(reference))
            return repo
    except (hub.lib.pygit2.GitError, KeyError, ValueError):
        if hub.lib.os.path.isdir(clone_dir) and hub.lib.os.listdir(clone_dir):
            raise
            await hub.log.debug(f"Directory {clone_dir} exists and is not empty. Removing existing files.")
            hub.lib.shutil.rmtree(clone_dir)
            hub.lib.os.makedirs(clone_dir, exist_ok=True)

        await hub.log.debug(f"Cloning salt from '{salt_repo_url}'")
        repo = hub.lib.pygit2.clone_repository(salt_repo_url, clone_dir, checkout_branch=salt_version, depth=1)
        await hub.log.debug(f"Checking out tag, branch, or version: {salt_version}")
        repo.checkout(repo.lookup_reference(f"refs/tags/{salt_version}"))
        return repo


async def find_matching_files(hub, repo, pattern: str, **kwargs) -> set[str]:
    """
    Find all files in the salt repo the match the given fnmatch pattern
    """
    matching_files = set()

    # Get the tree object for the repository's HEAD commit
    head_commit = repo.revparse_single("HEAD")
    head_tree = head_commit.tree
    await hub.log.debug(f"Searching for files matching pattern: {pattern}")

    # Recursively walk through the tree, looking for files that match the pattern
    async def _walk_tree(tree, path_prefix=""):
        for entry in tree:
            if entry.type == hub.lib.pygit2.GIT_OBJ_TREE:
                # If the entry is a tree (directory), recurse into it
                subtree = repo.get(entry.id)
                await _walk_tree(subtree, path_prefix + entry.name + ".")
            elif entry.type == hub.lib.pygit2.GIT_OBJ_BLOB:
                # If the entry is a blob (file), check if it matches the pattern
                stem, ext = hub.lib.os.path.splitext(entry.name)
                if ext != ".py":
                    continue
                file_path = path_prefix + stem
                await hub.log.trace(f"Checking if {file_path} matches {pattern}")
                if hub.lib.fnmatch.fnmatch(file_path, pattern):
                    # Convert file path to dot-delimited format and remove the .py extension
                    module_path = file_path.replace(hub.lib.os.path.sep, ".")
                    matching_files.add(module_path)
                    await hub.log.debug(f"Found matching file: {module_path}")

    await _walk_tree(head_tree)
    await hub.log.debug(f"Total matching files found: {len(matching_files)}")
    return matching_files


async def index_python_file_imports(hub, repo) -> dict[str, set[str]]:
    python_imports_index: dict[str, set[str]] = {}

    # Get the tree object for the repository's HEAD commit
    head_commit = repo.revparse_single("HEAD")
    head_tree = head_commit.tree

    # Recursively walk through the tree, looking for Python files
    async def _walk_tree(tree, path_prefix=""):
        # Ensure that the path prefix does not start with a dot
        if path_prefix.startswith("."):
            return
        for entry in tree:
            if entry.type == hub.lib.pygit2.GIT_OBJ_TREE:
                # If the entry is a tree (directory), recurse into it
                subtree = repo.get(entry.id)
                new_path_prefix = path_prefix + entry.name + hub.lib.os.path.sep
                await _walk_tree(subtree, new_path_prefix)
            elif entry.type == hub.lib.pygit2.GIT_OBJ_BLOB and entry.name.endswith(".py"):
                # If the entry is a Python file, analyze it
                stem, ext = hub.lib.os.path.splitext(entry.name)
                file_path = path_prefix + entry.name
                module_path = (path_prefix + stem).replace(hub.lib.os.path.sep, ".")
                await hub.log.trace(f"Analyzing Python file: '{file_path}' module: '{module_path}'")
                python_imports_index[module_path] = await _analyze_python_file(repo, file_path)

    async def _analyze_python_file(repo, file_path: str) -> set[str]:
        # Get the file content from the repository
        try:
            blob = repo.revparse_single("HEAD:" + file_path)
        except Exception as e:
            await hub.log.trace(f"Error analyzing python file '{file_path}': {e}")
            return set()
        file_content = blob.data.decode("utf-8")

        # Parse the file content using the ast module
        try:
            tree = hub.lib.ast.parse(file_content)
        except Exception as e:
            await hub.log.trace(f"Error parsing python file '{file_path}': {e}")
            return set()

        # Extract imports
        imports: set[str] = set()
        for node in hub.lib.ast.walk(tree):
            if isinstance(node, hub.lib.ast.Import):
                for alias in node.names:
                    imports.add(alias.name)
            elif isinstance(node, hub.lib.ast.ImportFrom):
                if node.module:
                    imports.add(node.module)

        return imports

    await _walk_tree(head_tree)

    total_entries = len(python_imports_index)
    total_refs = sum(len(refs) for refs in python_imports_index.values())
    average_refs = total_refs / total_entries if total_entries > 0 else 0
    await hub.log.debug(f"Total entries in Python imports index: {total_entries}")
    await hub.log.debug(f"Average number of references per entry: {average_refs:.2f}")

    return python_imports_index
