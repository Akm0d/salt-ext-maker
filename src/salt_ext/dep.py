async def match(hub, modules: list[str], index: dict[str, set[str]]) -> set[str]:
    """
    Given a list of modules, gather a combined set of their dependencies
    """
    # Start with the original matches
    dependencies = set(modules)

    for match in modules:
        if match in index:
            # Add the imported modules to the extended matches
            imports = index[match]
            dependencies.update(imports)
        elif f"salt.{match}" in index:
            # Add the imported modules to the extended matches
            imports = index[f"salt.{match}"]
            dependencies.update(imports)

    return dependencies


async def split(hub, deps: set[str], **kwargs) -> set[str]:
    """
    Identify all the dependencies that come from within salt
    """
    # TODO handle relative imports beneath salt.*
    #   This could easily come from saltext.cli.__main__.SALT_LOADERS + the unique ones in this list
    salt_deps = {
        dep
        for dep in deps
        if dep.startswith("salt.")
        or dep.startswith("tests.")
        or dep.startswith("tools.")
        or dep.startswith("pkg.")
        or dep.startswith("templates.")
        or dep.startswith("doc.")
        or dep.startswith("scripts.")
        or dep.startswith(".")
        or dep in ("setup", "noxfile")
    }
    await hub.log.debug(f"Salt deps: {len(salt_deps)}")
    return salt_deps


async def build(hub, deps: set[str], clone_dir: str, **kwargs) -> list[str]:
    """
    Create a new requirements list by concatenating all .txt and .in files in the
    requirements directory of the repository and matching packages with the provided set.
    """
    await hub.log.debug(f"Building requirements from {len(deps)} deps")
    requirements_dir = hub.lib.os.path.join(clone_dir, "requirements")
    master_requirements = []

    # Read all .txt and .in files in the requirements directory
    for filename in hub.lib.os.listdir(requirements_dir):
        if filename.endswith((".txt", ".in")):
            with open(hub.lib.os.path.join(requirements_dir, filename)) as f:
                for line in f:
                    if not line.startswith("#"):
                        master_requirements.append(line.strip())

    # Function to strip non-alphabetic characters
    def _strip_non_alpha(s):
        return "".join(filter(str.isalpha, s))

    # Create a list of stripped requirement lines
    stripped_master_requirements = [
        _strip_non_alpha(req_line.lower().split(" ")[0]) for req_line in master_requirements
    ]

    # Filter the master list to include only the packages in the provided set
    matched_requirements = set()
    for dep in deps:
        # Skip builtin modules
        if dep in ("sys", "time", "re", "nt"):
            continue
        stripped_dep = _strip_non_alpha(dep.lower())
        for stripped_req_line, req_line in zip(stripped_master_requirements, master_requirements, strict=True):
            if stripped_dep in stripped_req_line:
                matched_requirements.add(req_line)
                await hub.log.debug(f"Matched: {dep} in {req_line}")
                # Stop searching once a match is found for this dependency
                break

    # Make a case-insensitive sorted list of the requirements
    return sorted(matched_requirements, key=str.casefold)


async def tests(hub, matches: set[str], index: dict[str, set[str]]) -> set[str]:
    """
    Find all test modules that import a module from the given set of matches.
    """
    await hub.log.debug(f"Finding tests for {len(matches)} matches")

    test_modules = {module for module in index if module.startswith("tests.")}
    await hub.log.debug(f"Total test modules found: {len(test_modules)}")

    relevant_tests = set()
    for test_module in test_modules:
        test_imports = index[test_module]
        await hub.log.trace(f"Checking test module '{test_module}' with imports {test_imports}")

        for imp in test_imports:
            if imp in matches:
                await hub.log.debug(f"Match found: '{test_module}' imports '{imp}'")
                relevant_tests.add(test_module)

    for match in matches:
        if match.startswith("tests."):
            relevant_tests.add(match)

    await hub.log.debug(f"Relevant tests: {len(relevant_tests)}")
    return relevant_tests
