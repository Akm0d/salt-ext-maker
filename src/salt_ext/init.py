async def cli(hub):
    """
    This is the entrypoint for the async code in your project
    """
    kwargs = hub.lib.cpop.data.unfreeze(hub.OPT.salt_ext)

    # Clone salt with the specified info
    repo = await hub.salt_ext.repo.clone(**kwargs)

    # Parse the given target to find all salt resources that match the glob
    matches = await hub.salt_ext.repo.find_matching_files(repo, **kwargs)
    if not matches:
        return

    # Index the tests by scanning all the python files and marking which modules each one imports
    index = await hub.salt_ext.repo.index_python_file_imports(repo)

    # Find all dependencies of all the matching files
    deps = await hub.salt_ext.dep.match({m for m in matches if not m.startswith("tests.")}, index)
    salt_deps = await hub.salt_ext.dep.split(deps)

    # Separate the 3rd party modules from internal salt imports
    modules = deps - salt_deps

    # Identify the PyPi requirements associated with the given modules
    # TODO this needs to go into a pyproject.toml
    requirements = await hub.salt_ext.dep.build(modules, **kwargs)
    await hub.log.debug("requirements/base.txt:\n" + "\n".join(requirements))

    # Find all tests that import a module from salt_dep matches
    relevant_tests = await hub.salt_ext.dep.tests(matches, index)

    # Find dependencies for tests
    test_salt_deps = await hub.salt_ext.dep.match({m for m in matches if m.startswith("tests.")}, index)

    # Separate the 3rd party test modules from internal salt imports
    test_modules = test_salt_deps - salt_deps

    # Identify the PyPi requirements associated with the given test modules
    test_requirements = await hub.salt_ext.dep.build(test_modules, **kwargs)
    await hub.log.debug("requirements/test.txt:\n" + "\n".join(test_requirements))

    # Copy code to new salt extension using https://github.com/saltstack/salt-extension
    args = await hub.salt_ext.create.args(
        salt_modules=salt_deps,
        test_modules=relevant_tests,
        salt_module_reqs=requirements,
        test_module_reqs=test_requirements,
        # TODO need to pass target directory
        **kwargs,
    )
    # TODO make dry_run configurable
    await hub.salt_ext.create.extension(*args, dry_run=True)

    # TODO Generate docs for the extracted modules with sphinx


# Gareth's tool to do the migration preserving history for specific files in salt
# https://github.com/salt-extensions/extension_migration/tree/main

# Community tool for boilerplate code
# https://github.com/lkubb/salt-extension-copier

# Pedro's tool for boilerplate code
# https://github.com/saltstack/salt-extension/blob/main/src/saltext/cli/__main__.py

# Repository for modules that have been removed
# https://github.com/saltstack/great-module-migration
