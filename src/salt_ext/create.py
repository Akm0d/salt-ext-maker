async def __init__(hub):
    await hub.pop.sub.add(python_import="saltext", sub=hub.lib)
    hub.lib.importlib.import_module("saltext.cli.__main__")


async def args(
    hub,
    salt_modules: set[str],
    test_modules: set[str],
    salt_module_reqs: set[str],
    test_module_reqs: set[str],
    pattern: str,
    # TODO target_dir:str,
    **kwargs,
) -> list[str]:
    """
    Based on all collected and indexed information, create args for the salt-extension cli
    """
    result = []

    # Assume that the author information will be the same as local git user
    try:
        author_name = hub.lib.subprocess.run(
            ["git", "config", "--global", "user.name"],
            capture_output=True,
            text=True,
        ).stdout.strip()
        result.append(f"--author='{author_name}'")
    except Exception as e:
        await hub.log.error(f"Unable to gather author name from git config: {e}")

    try:
        author_email = hub.lib.subprocess.run(
            ["git", "config", "--global", "user.email"],
            capture_output=True,
            text=True,
        ).stdout.strip()
        result.append(f"--author-email='{author_email}'")
    except Exception as e:
        await hub.log.error(f"Unable to gather author email from git config: {e}")

    # Create the package name from the input pattern used to run the script
    package_name = hub.lib.re.sub(r"[^a-z0-9]+", "-", f"salt-ext-{pattern.lower()}").rstrip("-")
    result.append(f"--name='{package_name}'")

    # TODO --salt-version by parsing cloned salt repo
    # TODO --summary that includes the commit sha of the version of salt the extension
    #       came from and all modules included
    # git-filter-repo -- to preserve the history of the file in extension
    # TODO --loader based on {m.split(".")[0] for m in salt_modules} matched to the list of LOADERS
    # TODO --dest as a new option to pass straight through to salt-extension

    return result


async def extension(hub, *args, dry_run: bool, **kwargs):
    """
    Call salt-extension to create boilerplate code for the extracted modules
    """
    if dry_run:
        await hub.lib.aioconsole.aprint(f"salt-extension {' '.join(args)}")
    else:
        return hub.lib.saltext.cli.__main__.main(args)


async def copy(hub, **kwargs):
    """
    Copy relevant files and modules from the cloned salt repo to the new extension
    """
