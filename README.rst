========
salt-ext
========

.. image:: https://img.shields.io/badge/made%20with-pop-teal
   :alt: Made with pop, a Python implementation of Plugin Oriented Programming
   :target: https://pop.readthedocs.io/

.. image:: https://img.shields.io/badge/made%20with-python-yellow
   :alt: Made with Python
   :target: https://www.python.org/


Automate the creation of Salt extensions with relevant code from previous salt versions

About
=====

This project provides a tool for SaltStack users to automate the process of creating Salt extensions,
particularly useful in light of the upcoming changes in Salt 3008 where the codebase will be significantly trimmed down.
Users can extract specific modules, their dependencies, and associated tests from any version of the Salt codebase,
enabling them to reintegrate functionality that may have been removed in newer versions.
The tool clones the specified Salt repository,
parses the target modules to identify all related Salt resources and third-party dependencies,
and then filters out relevant tests.
It utilizes the salt-extension tool to initialize a new extension structure and populates it with the identfified modules and tests.
Additionally, it generates a requirements file that includes all necessary third-party dependencies for the extension.
This project streamlines the process of creating customized Salt extensions, allowing developers to easily modularize and manage their SaltStack configurations and states, even as the core Salt codebase evolves.

What is POP?
------------

This project is built with `pop <https://pop.readthedocs.io/>`__, a Python-based
implementation of *Plugin Oriented Programming (POP)*. POP seeks to bring
together concepts and wisdom from the history of computing in new ways to solve
modern computing problems.

For more information:

* `Intro to Plugin Oriented Programming (POP) <https://pop-book.readthedocs.io/en/latest/>`__
* `pop-awesome <https://gitlab.com/vmware/pop/pop-awesome>`__
* `pop-create <https://gitlab.com/vmware/pop/pop-create/>`__

Getting Started
===============

Prerequisites
-------------

* Python 3.10+
* git *(if installing from source, or contributing to the project)*

Installation
------------

.. note::

   If wanting to contribute to the project, and setup your local development
   environment, see the ``CONTRIBUTING.rst`` document in the source repository
   for this project.

If wanting to use ``salt-ext``, you can do so by either
installing from PyPI or from source.

Install from PyPI
+++++++++++++++++

.. code-block:: bash

   pip install salt-ext-maker

Install from source
+++++++++++++++++++

.. code-block:: bash

   # clone repo
   git clone git@Akm0d/salt-ext-maker.git
   cd salt-ext-maker

   # Setup venv
   python3 -m venv .venv
   source .venv/bin/activate
   pip install -e .

Usage
=====

You only need to specify a pattern of salt modules to target.
The tool will clone the Salt repository with the configured version or tag.
Then it will parse the whole module to identify modules that match the pattern and all salt modules they depend on.
Then it will identify tests that use any of those modules.
Then it parses the imports of the relevant tests and modules and creates test requirements and base requirements with version ranges from salt
Then a boilerplate project is created with the salt-extension tool.
Then all the files are copied to the boilerplate project.

Examples
--------

Rip out all cloud related code, tests, and requirements from salt 3005 and make it into a standalone extension

.. code-block:: bash

   hub salt_ext.init.cli "*.cloud.*" --log-level=debug --salt-version=3005


Roadmap
=======

Reference the `open issues <https://gitlab.com/akm0d/salt-ext-maker/issues>`__ for a list of
proposed features (and known issues).

Acknowledgements
================

* `Img Shields <https://shields.io>`__ for making repository badges easy.
